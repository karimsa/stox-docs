# Where does the data come from?

The data fetched by the `Stock` class of stox is sorted into two categories:
_real-time intraday data_ and _historical intraday data_.

Real-time intraday data is available free-of-charge by many online services
(as well as trading platforms) including Yahoo! Finance, Google Finance, and
MSN Money. The one used by stox (by default) is Yahoo! Finance. However, this
data source can be over-ridden if you wish to use your own data source.

The process for historical data is a bit more complicated.

There are 85 properties of stocks available to fetch, and the maximum size of an
individual property value is approximately 8 bytes (assuming that all properties
are saved as numbers - which most are). Therefore, the maximum amount of memory
a single stock update can take is 680 bytes. The network retains second-by-second
stock data and therefore for a single day, it would store 58, 752, 000 bytes (58.75
MB). Extrapolating further, a year's worth of intraday data would take up ~21 GB of
disk space.

Each year's data is split off into 21 GB storage files and uploaded to a P2P network
using [dat](http://dat-data.com/). A UDP tracker is available at 'tracker.getstox.co'
that provides the mapping of a symbol + a year to the respective dat link as well as a
table of values mapping the byte to byte sectors representing each week of data.

_A sample request to `udp://tracker.getstox.co:8000/` with the datum `NASDAQ:GOOG@2015`_:

```
{
  "link": "dat://[dat-link]",
  "table": [
    [0, 85],
    [86, 171],
    ...
  ]
}
```

The `table` property is a 2D array mapping of the byte-to-byte sector of the data file
where the stock data of that particular week can be found (i.e. index _n_ of `table`
should refer to the data from days _7n_ to _7(n + 1) - 1_). The first integer in the
sub-array refers to the starting byte of that week's data and the second integer
refers to the ending byte of the data. This allows us to create a random-access stream
to stream the data.