# Observing Price Updates

To follow a stock's price changes and place orders on it, the `Stock` component should be used. This component is extended from type `Observable` (of [rxjs](http://reactivex.io/rxjs)).

The constructor for `Stock` takes a single argument, a string of the format `market:symbol` where `market` represents the stock market within which the symbol `symbol` is being traded (and therefore should be watched).

_An example of code to print price changes of the symbol 'GOOG' under the market of 'NASDAQ':_

```javascript
import {Stock} from 'stox';

(new Stock('NASDAQ:GOOG'))
  // the subscribe method is of the class 'Observable' and simply calls
  // a callback on all data events
  .subscribe((price) => console.log('$ %s', price));
```