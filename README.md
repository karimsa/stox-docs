# [stox docs](http://stox.rtfd.org/) [![Documentation Status](https://readthedocs.org/projects/stox/badge/?version=latest)](http://stox.readthedocs.org/en/latest/?badge=latest)

documentation for stox.

## Credits

Built with [MkDocs](http://www.mkdocs.org/).
Hosted on [ReadTheDocs](http://readthedocs.org/).

Written by Karim Alibhai and Valen Varangu-Booth.